drop trigger if exists update_user_role_change_at on aula.user_role;
create trigger update_user_role_change_at before update on aula.user_role for each row execute procedure aula.update_changed_column();
