create table if not exists aula.user_role (
    created_by  bigint      not null default request.user_id() references aula.users (id),
    created_at  timestamptz not null default now(),
    changed_by  bigint      not null default request.user_id() references aula.users (id),
    changed_at  timestamptz not null default now(),
    school_id    bigint         references aula.school (id),
    user_id      bigint         not null references aula.users (id) on delete cascade,
    role_id 	 bigint         not null references aula.roles (id),
    idea_space   bigint         references aula.idea_space (id),
    unique (user_id, role_id, idea_space)
);
