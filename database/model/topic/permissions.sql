alter table aula.topic enable row level security;

drop policy if exists topic_select on aula.topic;
create policy topic_select
  on aula.topic
  for SELECT
  using 
  (
    aula.is_admin(school_id) or
    aula.from_school(school_id)
  );

drop policy if exists topic_insert on aula.topic;
create policy topic_insert
  on aula.topic
  for INSERT
  with check 
  (
    created_by = request.user_id() and 
    (
      aula.is_admin(school_id) or
      aula.is_school_admin(school_id) or
      aula.check_permission('create_topic', request.user_id(), idea_space, school_id)
    )
  );

drop policy if exists topic_update on aula.topic;
create policy topic_update
  on aula.topic
  for UPDATE
  using 
  (
    aula.is_admin(school_id) or
    aula.is_school_admin(school_id) or
    -- aula.is_owner(created_by) or
    aula.check_permission('edit_topic', request.user_id(), idea_space, school_id) or
    aula.check_permission('change_topic_phase', request.user_id(), idea_space, school_id)
  )
  with check 
  (
    aula.is_admin(school_id) or
    aula.is_school_admin(school_id) or  
    -- aula.is_owner(created_by) or
    aula.can_update_topic(topic,
                          aula.check_permission('edit_topic', request.user_id(), idea_space, school_id),
                          aula.check_permission('change_topic_phase', request.user_id(), idea_space, school_id))
  );

drop policy if exists topic_delete on aula.topic;
create policy topic_delete
  on aula.topic
  for DELETE
  using 
  (
    aula.is_admin(school_id) or
    aula.is_school_admin(school_id) or
    aula.check_permission('delete_topic', request.user_id(), idea_space, school_id)
  );
