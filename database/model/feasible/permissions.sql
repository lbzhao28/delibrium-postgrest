alter table aula.feasible enable row level security;

-- SELECT
drop policy if exists feasible_select on aula.feasible;
create policy feasible_select 
  on aula.feasible
  for SELECT
  using 
  (
    aula.is_admin(school_id) or
    aula.is_school_admin(school_id) or
    aula.from_school(school_id)
  );

-- INSERT
drop policy if exists feasible_insert on aula.feasible;
create policy feasible_insert
  on aula.feasible
  for INSERT 
  with check 
  (
    created_by = request.user_id() and
    (
      aula.is_admin(school_id) or
      aula.is_school_admin(school_id) or     
      aula.check_permission('approve_idea', request.user_id(), (select idea_space from aula.idea where id = idea), school_id)
    )
  );

-- UPDATE
drop policy if exists feasible_update on aula.feasible;
create policy feasible_update 
  on aula.feasible 
  for UPDATE 
  using 
  (
    aula.is_admin(school_id) or
    aula.is_school_admin(school_id) or
    aula.is_owner(created_by) or
    aula.check_permission('approve_idea', request.user_id(), (select idea_space from aula.idea where id = idea), school_id)
  ) 
  with check 
  (
    aula.is_admin(school_id) or
    aula.is_school_admin(school_id) or
    aula.is_owner(created_by) or
    aula.check_permission('approve_idea', request.user_id(), (select idea_space from aula.idea where id = idea), school_id)
  );

-- DELETE
drop policy if exists feasible_delete on aula.feasible;
create policy feasible_delete
  on aula.feasible 
  for DELETE 
  using 
  (
    aula.is_admin(school_id) or
    aula.is_school_admin(school_id)
  ); 
