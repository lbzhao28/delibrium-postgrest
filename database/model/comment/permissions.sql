alter table aula.comment enable row level security;

-- SELECT
drop policy if exists comment_select on aula.comment;
create policy comment_select
  on aula.comment 
  for SELECT 
  using 
  (
    aula.is_admin(school_id) or 
    aula.from_school(school_id)
  );

-- INSERT
drop policy if exists comment_insert on aula.comment;
create policy comment_insert 
  on aula.comment 
  for INSERT 
  with check 
  (
    created_by = request.user_id() and
    ( 
      aula.is_admin(school_id) or
      aula.is_school_admin(school_id) or 
      (
        aula.from_school(school_id) and 
        aula.check_permission('create_comment', request.user_id(), (select idea_space from aula.idea where id = parent_idea), school_id)
      )
    )
  );

-- UPDATE
drop policy if exists comment_update on aula.comment;
create policy comment_update
  on aula.comment 
  for UPDATE 
  using 
  (
    aula.is_admin(school_id) or
    aula.is_school_admin(school_id) or
    aula.is_owner(created_by) or 
    (
      aula.from_school(school_id) and 
      aula.check_permission('edit_comment', request.user_id(), (select idea_space from aula.idea where id = parent_idea), school_id) or
      aula.check_permission('delete_comment', request.user_id(), (select idea_space from aula.idea where id = parent_idea), school_id)
    )
  ) 
  with check 
  (
    aula.is_admin(school_id) or
    aula.is_school_admin(school_id) or
    aula.is_owner(created_by) or
    (
      aula.from_school(school_id) and 
      is_deleted is not true and 
      aula.check_permission('edit_comment', request.user_id(), (select idea_space from aula.idea where id = parent_idea), school_id)
    ) or
    (
      aula.from_school(school_id) and
      is_deleted is true and
      aula.check_permission('delete_comment', request.user_id(), (select idea_space from aula.idea where id = parent_idea), school_id)
    )
  );

-- DELETE
drop policy if exists comment_delete on aula.comment;
create policy comment_delete
  on aula.comment
  for DELETE 
  using 
  (
    aula.is_admin(school_id)
  );