alter table aula.idea enable row level security;

-- SELECT
drop policy if exists idea_select on aula.idea;
create policy idea_select
  on aula.idea 
  for select
  using 
  (
    aula.is_admin(school_id) or
    aula.is_school_admin(school_id) or 
    aula.from_school(school_id)
  );

-- INSERT
drop policy if exists idea_insert on aula.idea;
create policy idea_insert
  on aula.idea 
  for INSERT 
  with check 
  (
    created_by = request.user_id() and
    (
      aula.is_admin(school_id) or
      aula.is_school_admin(school_id) or
      aula.check_permission('create_idea', request.user_id(), idea_space, school_id) or
        (
          topic is not Null and
          aula.check_permission('add_topic_idea', request.user_id(), idea_space, school_id)
        )
    )
  );

-- UPDATE
drop policy if exists idea_update on aula.idea;
create policy idea_update 
  on aula.idea 
  for UPDATE 
  using 
  (
    aula.is_admin(school_id) or 
    aula.is_school_admin(school_id) or
    aula.is_owner(created_by) or 
    aula.check_permission('edit_idea', request.user_id(), idea_space, school_id) or
    aula.check_permission('edit_topic', request.user_id(), idea_space, school_id) or
    aula.check_permission('mark_idea_winner', request.user_id(), idea_space, school_id)
  ) 
  with check 
  (
    aula.is_admin(school_id) or
    aula.is_school_admin(school_id) or 
    aula.can_update_idea(idea,
                         aula.check_permission('edit_idea', request.user_id(), idea_space, school_id),
                         aula.check_permission('edit_topic', request.user_id(), idea_space, school_id),
                         aula.check_permission('mark_idea_winner', request.user_id(), idea_space, school_id))
  );

-- DELETE
drop policy if exists idea_delete on aula.idea;
create policy idea_delete
  on aula.idea 
  for delete 
  using 
  (
    aula.is_admin(school_id) or 
    aula.is_school_admin(school_id) or
    aula.is_owner(created_by) or 
    aula.check_permission('delete_idea', request.user_id(), idea_space, school_id)
  ); 
