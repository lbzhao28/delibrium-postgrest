alter table aula.users enable row level security;

-- SELECT
drop policy if exists users_select on aula.users;
create policy users_select
  on aula.users
  for SELECT
  using 
  (
    aula.is_admin(school_id) or 
    aula.from_school(school_id) or 
    aula.is_owner(id)
  );

-- INSERT
drop policy if exists users_insert on aula.users;
create policy users_insert
  on aula.users
  for INSERT
  with check 
  (
    (
      aula.is_admin(school_id) or 
      aula.is_school_admin(school_id) or
      aula.check_permission('create_user', request.user_id(), Null, school_id)
    )
  );

-- UPDATE
drop policy if exists users_update on aula.users;
create policy users_update
  on aula.users
  for UPDATE
  using 
  (
    aula.is_admin(school_id) or
    school_id = request.school_id() and 
    (
      aula.is_school_admin(school_id) or 
      aula.is_owner(id) or 
      aula.check_permission('edit_user', request.user_id(), Null, school_id)
    )
  )
  with check 
  (
    aula.is_admin(school_id) or
    school_id = request.school_id() and
    (
      aula.is_school_admin(school_id) or  
      aula.is_owner(id) or 
      aula.check_permission('edit_user', request.user_id(), Null, school_id)
    )
  );

-- DELETE
drop policy if exists users_delete on aula.users;
create policy users_delete
  on aula.users
  for DELETE
  using 
  (
    aula.is_admin(school_id) or
    aula.is_school_admin(school_id) or 
    aula.check_permission('delete_user', request.user_id(), Null, school_id)
  );

\ir ../secure/user_login/permissions.sql
