alter table aula.idea_space enable row level security;

drop policy if exists idea_space_select on aula.idea_space;
create policy idea_space_select
  on aula.idea_space
  for SELECT
  using 
  (
    aula.is_admin(school_id) or
    aula.from_school(school_id)
  );

drop policy if exists idea_space_insert on aula.idea_space;
create policy idea_space_insert
  on aula.idea_space
  for INSERT
  with check 
  (
    created_by = request.user_id() and
    (
      aula.is_admin(school_id) or
      aula.is_school_admin(school_id) or
      aula.check_permission('create_room', request.user_id(), null, school_id)
    )
  );

drop policy if exists idea_space_update on aula.idea_space;
create policy idea_space_update
  on aula.idea_space
  for UPDATE
  using 
  (
    aula.is_admin(school_id) or
    aula.is_school_admin(school_id) or
    aula.check_permission('edit_room', request.user_id(), null, school_id)
  );

drop policy if exists idea_space_delete on aula.idea_space;
create policy idea_space_delete
  on aula.idea_space
  for DELETE
  using 
  (
    aula.is_admin(school_id) or
    aula.is_school_admin(school_id) or
    aula.check_permission('delete_room', request.user_id(), null, school_id)
  );