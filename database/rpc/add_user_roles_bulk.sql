create or replace function aula.add_user_roles_bulk(schoolid bigint, userids bigint[], roleid bigint, ideaspace bigint)
  returns boolean
  language plpython3u
  set search_path = public, aula
  as $$

insert_string = "INSERT INTO aula.user_role (school_id, user_id, role_id, idea_space) VALUES "
insert_string_values = []

if ideaspace:
  for user_id in userids:
    string = "({},{},'{}',{})".format(schoolid, user_id, roleid, ideaspace)
    insert_string_values.append(string)
else:
  for user_id in userids:
    string = "({},{},'{}',null)".format(schoolid, user_id, roleid)
    insert_string_values.append(string)

insert_string += ",".join(insert_string_values)

insert_string += "ON CONFLICT (user_id, role_id, idea_space) DO NOTHING"

plpy.execute(insert_string)

return True
$$;
