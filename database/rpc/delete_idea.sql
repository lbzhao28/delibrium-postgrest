create or replace function aula.delete_idea(school_id bigint, idea_id bigint)
    returns json
    language plpython3u
as $$
    import json

    res_school_id = plpy.execute("select current_setting('request.jwt.claim.school_id');")
    if len(res_school_id) == 0:
        plpy.error('Current user is not associated with a school.', sqlstate='PT401')

    res_calling_user_id = plpy.execute("select current_setting('request.jwt.claim.user_id');")
    if len(res_calling_user_id) == 0:
        plpy.error('Did not find user associated with this request.', sqlstate='PT401')
    calling_user_id = res_calling_user_id[0]['current_setting']

    school_id = res_school_id[0]['current_setting']

    check_admin = plpy.prepare("select aula.is_admin($1);", ["bigint"])
    res_is_admin = plpy.execute(check_admin, [school_id])

    check_space_id_of_idea = plpy.prepare("select idea_space from aula.idea where id = $1;", ["bigint"])
    space_id_of_idea = plpy.execute(check_space_id_of_idea, [idea_id])
    if len(space_id_of_idea) == 0:
        plpy.error('Did not find idea associated with this request.', sqlstate='PT401')
    space_id = space_id_of_idea[0]['idea_space']
        
    calling_user_id = res_calling_user_id[0]['current_setting']
    check_delete_permission = plpy.prepare("select aula.check_permission('delete_idea', $1, $2, $3)", ["bigint", "bigint", "bigint"])
    delete_permission = plpy.execute(check_delete_permission, [calling_user_id, space_id, school_id])

    is_admin = res_is_admin[0]['is_admin']
    if is_admin or delete_permission:
      if delete_permission and is_admin == False:
        plpy.execute('set "request.jwt.claim.is_admin" TO "True"')
      
      get_comment_id = plpy.prepare('select id from aula.comment where school_id = $1 and parent_idea = $2', ["bigint", "bigint"])
      comments = plpy.execute(get_comment_id, [school_id, idea_id])
      delete_comment_vote = plpy.prepare('delete from aula.comment_vote where comment = $1', ["bigint"])
      for comment in comments:
        # Delete comments
        plpy.execute(delete_comment_vote, [comment['id']])

      delete_comment = plpy.prepare('delete from aula.comment where parent_idea = $1', ["bigint"]) 
      plpy.execute(delete_comment, [idea_id])
      # Delete idea votes
      delete_idea_vote = plpy.prepare('delete from aula.idea_vote where idea = $1', ["bigint"])
      plpy.execute(delete_idea_vote, [idea_id])
      # Delete idea like
      delete_idea_like = plpy.prepare('delete from aula.idea_like where idea = $1', ["bigint"])
      plpy.execute(delete_idea_like, [idea_id])
      # Delete idea feasibility
      delete_idea_feasibility = plpy.prepare('delete from aula.feasible where idea = $1', ["bigint"])
      plpy.execute(delete_idea_feasibility, [idea_id])
      # Delete idea
      delete_idea = plpy.prepare('delete from aula.idea where id = $1', ["bigint"])
      plpy.execute(delete_idea, [idea_id])
      
      # Revoke admin permissions if not admin
      if delete_permission and is_admin == False:
        plpy.execute('set "request.jwt.claim.is_admin" TO "False"')
    return json.dumps([])
$$;

grant execute on function aula.delete_idea (bigint, bigint) to aula_authenticator;
