create or replace function aula.check_usernames(usernames text[])
    returns json
    language plpython3u
as $$
    import json

    plpy.execute('set "request.jwt.claim.is_admin" TO "True"')
    
    usernames_result = []

    for username in usernames:
        username_db_count_plan = plpy.prepare("select count(*) from aula.users where username = $1", ["text"])
        username_db_count = plpy.execute(username_db_count_plan, [username])[0]['count']

        if username_db_count > 0 or username in usernames_result:

            if username[-3:].isdigit():
                index = int(username[-3:])
                first_username_part = username[:-3]
            elif username[-2:].isdigit():
                index = int(username[-2:])
                first_username_part = username[:-2]
            elif username[-1].isdigit():
                index = int(username[-1])
                first_username_part = username[:-1]
            else:
                index = 0
                first_username_part = username

            index += 1

            while True:
                username_suggestion = first_username_part + str(index)
                username_suggestion_count = plpy.execute(username_db_count_plan, [username_suggestion])[0]['count']

                if username_suggestion_count == 0 and username_suggestion not in usernames_result:
                    usernames_result.append(username_suggestion)
                    break
                else:
                    index += 1

        else:
            usernames_result.append(username)

    return json.dumps(usernames_result)
$$;

grant execute on function aula.check_usernames(text[]) to aula_authenticator;
