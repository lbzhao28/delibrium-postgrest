create or replace function aula.delete_user(schoolid bigint, userid bigint)
  returns void
  language plpython3u
  set search_path = public, aula
as $$
import json

current_user_id = plpy.execute("select current_setting('request.jwt.claim.user_id', true)")[0]['current_setting']

if (str(current_user_id) == str(userid)):
  return
else:
  # Get school id from JWT
  res_school_id = plpy.execute(
      "select current_setting('request.jwt.claim.school_id');"
  )
  if len(res_school_id) == 0:
      plpy.error('Current user is not associated with a school.', sqlstate='PT401')
  school_id = res_school_id[0]['current_setting']

  # Check if user is admin
  is_admin_plan = plpy.prepare(
    "select aula.is_admin($1);", ["bigint"]
  )
  is_admin = plpy.execute(is_admin_plan, [school_id])

  # Check if user is school_admin
  is_school_admin_plan = plpy.prepare(
    "select aula.is_school_admin($1);", ["bigint"]
  )
  is_school_admin = plpy.execute(is_school_admin_plan, [school_id])

  # Check if user has permission to delete user
  check_delete_user_permission = plpy.prepare("select aula.check_permission('delete_user', $1, null, $2)", ["bigint", "bigint"])
  has_delete_user_permission = plpy.execute(check_delete_user_permission, [current_user_id, school_id])

  if is_admin[0]['is_admin'] or is_school_admin[0]['is_school_admin'] or has_delete_user_permission[0]['check_permission']:
    plpy.execute('set "request.jwt.claim.is_admin" TO "True"')
  else:
    plpy.error('Insufficient rights to delete a user.', not is_school_admin[0]['is_school_admin'])

  login_id = plpy.execute('select user_login_id from aula.users where is_admin = false and id = {}'.format(userid))[0]['user_login_id']

  plpy.execute("update aula.users set "+
      "user_login_id = null,"+
      "first_name = 'deleted',"+
      "last_name = 'user',"+
      "username = 'deleted-' || id,"+
      "config = json_build_object('deleted', to_jsonb('t'::boolean)),"+
      "email = '',"+
      "picture = ''"+
      " where "+
      "id = {};".format(userid))
  plpy.execute('delete from aula_secure.user_login where id = {}'.format(login_id))
  plpy.execute('delete from aula.user_role where user_id = {}'.format(userid))
  return
$$;
