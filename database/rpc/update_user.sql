create or replace function aula.update_user(
    id bigint,
    first_name text,
    last_name text,
    email text default null
) returns void language plpython3u
as $$

# Get school id from JWT
res_school_id = plpy.execute(
    "select current_setting('request.jwt.claim.school_id');"
)
if len(res_school_id) == 0:
    plpy.error('Current user is not associated with a school.', sqlstate='PT401')
school_id = res_school_id[0]['current_setting']

# Get User id
res_calling_user_id = plpy.execute(
    "select current_setting('request.jwt.claim.user_id');"
  )
if len(res_calling_user_id) == 0:
    plpy.error('Did not find user associated with this request.', sqlstate='PT401')
calling_user_id = res_calling_user_id[0]['current_setting']

# Check if user is admin or school admin
is_admin_plan = plpy.prepare(
    "select aula.is_admin($1);", ["bigint"]
  )
is_admin = plpy.execute(is_admin_plan, [school_id])

is_school_admin_plan = plpy.prepare(
    "select aula.is_school_admin($1);", ["bigint"])
is_school_admin = plpy.execute(is_school_admin_plan, [school_id])

if not is_admin[0]['is_admin'] and not is_school_admin[0]['is_school_admin']:
  plpy.error('User must be admin to create users')

q = plpy.prepare("""update
aula.users set
    first_name=$1,
    last_name=$2,
    email=$3,
    changed_by=$4,
    changed_at=now()
where
    id=$5
returning user_login_id;""", ["text", "text", "text", "bigint", "bigint"])

res = plpy.execute(q, [first_name,
     last_name,
     email or '',
     calling_user_id,
     id]
)
login_id = res[0]['user_login_id']

$$;

grant execute on function aula.update_user(bigint,text,text,text) to aula_authenticator;