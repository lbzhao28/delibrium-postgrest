create or replace function aula.quorum_info(school_id bigint, space_id bigint default null)
  returns json
  language plpython3u
as $$
    import json
    import math

    get_config = plpy.prepare("""
        select config
        from aula.school
        where id = $1;
    """, ["bigint"])
    result = plpy.execute(get_config, [school_id])

    if len(result) == 0:
        plpy.error('School not found', sqlstate='PT404')
        return

    config = json.loads(result[0]['config'])

    if 'classQuorum' not in config:
        config = {
            'schoolQuorum': 30,
            'classQuorum': 30
        }

    if space_id is None:
        get_user_count = plpy.prepare("""
            select 
                count(distinct ur.user_id) 
            from 
                aula.user_role as ur 
                    join aula.roles as r on ur.role_id = r.id
                    join aula.users as us on ur.user_id = us.id,
                json_each_text(to_json(r.permissions)) 
            where 
                (value = '["all"]' or value = '["school"]' or value = '["school_and_idea_space"]')
                and key = 'vote_idea'
                and us.config->'deleted' is null
                and ur.school_id = $1;
        """, ["bigint"])
        usercount = plpy.execute(get_user_count, [school_id])[0]['count']
        config['totalVoters'] = usercount
        quorum_threshold = 0.01 * int(config['schoolQuorum'])
    else:
        get_user_count_space = plpy.prepare("""
            select 
                count(distinct ur.user_id) 
            from 
                aula.user_role as ur 
                    join aula.roles as r on ur.role_id = r.id 
                    join aula.users as us on ur.user_id = us.id,
                json_each_text(to_json(r.permissions)) 
            where 
                (value = '["all"]' or value = '["idea_space"]' or value = '["school_and_idea_space"]') 
                and key = 'vote_idea'
                and us.config->'deleted' is null 
                and ur.idea_space = $1 
                and ur.school_id = $2;
        """, ["bigint", "bigint"])
        usercount = plpy.execute(get_user_count_space, [space_id, school_id])[0]['count']
        config['totalVoters'] = usercount
        quorum_threshold = 0.01 * int(config['classQuorum'])

    config['totalVoters'] = usercount
    config['requiredVoteCount'] = max(1, math.ceil(usercount * quorum_threshold))

    return json.dumps(config)
$$;
