drop schema if exists default_data cascade;
create schema default_data;
grant usage on schema default_data to aula_authenticator;

\ir insert_default_roles.sql