create or replace function aula.can_update_topic(topic aula.topic, has_edit_topic_permission boolean, has_phase_change_permission boolean)
  returns boolean
  language plpython3u
as $$

  get_old_topic_phase = plpy.prepare("select phase from aula.topic where id = $1;", ["bigint"])
  rv = plpy.execute(get_old_topic_phase, [topic['id']])
  old_phase = rv[0]["phase"]

  if old_phase != topic["phase"]:
    if has_phase_change_permission:
      return True;

  if has_edit_topic_permission and old_phase == topic["phase"]:
    return True;

  plpy.error("Insufficent permission for updating topic", sqlstate='PT401')
  return False;
$$;
