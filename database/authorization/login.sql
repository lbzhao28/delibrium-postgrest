create or replace function aula.login(username text, password text)
  returns json
  language plpython3u
  set search_path = public, aula
as $$
  import json
  import time

  check_pass = plpy.prepare("select * from aula_secure.user_id($1, $2)", ["text", "text"])
  rv = plpy.execute(check_pass, [username, password])

  if rv:
    aula_user_id = int(rv[0]['uid'])
    session_count = int(rv[0]['sc'])
    last_login_plan = plpy.prepare("update aula_secure.user_login set last_login = now() where aula_user_id = $1 ", ["bigint"])
    plpy.execute(last_login_plan, [aula_user_id])
  else:
    plpy.error('authentication failed', sqlstate='PT401')
    return 'authentication failed'

  get_roles = plpy.prepare('select role_id, idea_space from aula.user_role where user_id = $1;', ["bigint"])
  rv = plpy.execute(get_roles, [aula_user_id])

  all_roles = []

  for r in rv:
    role = [r['role_id']]
    space = r['idea_space']
    if space:
      role += [space]
    else:
      role += [None]
    all_roles += [role]
  
  set_roles = plpy.prepare('set "request.jwt.claim.roles" TO "$1" ', ["text[]"])
  plpy.execute(set_roles, [all_roles])

  get_admin_rights = plpy.prepare('select is_admin, is_school_admin from aula.users where id = $1;', ["bigint"])
  rv =  plpy.execute(get_admin_rights, [aula_user_id])

  school_id_plan = plpy.prepare('select school_id from aula.users where id = $1', ["bigint"])
  school_id = plpy.execute(school_id_plan, [aula_user_id])[0]['school_id']

  is_admin = rv[0]['is_admin']
  is_school_admin = rv[0]['is_school_admin']
  set_admin = plpy.prepare('set "request.jwt.claim.is_admin" TO "$1" ', ["text"])
  set_school_admin = plpy.prepare('set "request.jwt.claim.is_school_admin" TO "$1" ', ["text"])
  plpy.execute(set_admin, [is_admin])
  plpy.execute(set_school_admin, [is_school_admin])

  token = {
    'is_admin': is_admin,
    'is_school_admin': is_school_admin,
    'school_id': school_id,
    'role': 'aula_authenticator',
    'roles': all_roles,
    'user_id': aula_user_id,
    'session_count': session_count,
    'exp': int(time.time()) + 60*60*24*31
  }

  sign_json = plpy.prepare('''select pgjwt.sign(
       $1, current_setting('app.jwt_secret')
       ) as token''', ["json"])
  rv = plpy.execute(sign_json, [json.dumps(token)])

  token = rv[0]['token']

  set_headers = '''set local "response.headers" = \'[{{"Access-Control-Expose-Headers": "Authorization"}}, {{"Authorization": "Bearer {}"}}]\' '''.format(token)
  plpy.execute(set_headers)

  user_data = {'roles': all_roles, 'school_id': school_id, 'user_id': aula_user_id, 'is_admin': is_admin, 'is_school_admin': is_school_admin }
  output = {'status': 'success', 'data': user_data}

  return json.dumps(output)
$$;

grant execute on function aula.login(text, text) to aula_authenticator;
