DO language plpython3u $$
    import subprocess
    import io

    ###
    ## Set variables
    ###
    
    base_dir = '/docker-entrypoint-initdb.d/'
    roles_init_path = base_dir + 'model/roles/init.sql'
    default_data_path = base_dir + 'libs/default_data/init.sql'
    user_role_init_path = base_dir + 'model/user_role/init.sql'
    rpc_init_path = base_dir + 'rpc/init.sql'
    authorization_init_path = base_dir + 'authorization/init.sql'

    # Helper function for printing output
    def monitor_process(process):
        for line in io.TextIOWrapper(process.stdout, encoding="utf-8"):
            plpy.info(line.rstrip())
        errcode = process.wait()
        return errcode

    ###
    ## Create roles and user_roles table
    ###

    plpy.info('--- Create roles and user_role ---')
   
    add_roles = subprocess.Popen(['psql', 'app', 'superuser', '-f', roles_init_path], stdout = subprocess.PIPE)
    add_roles_exit_code = monitor_process(add_roles)

    add_user_roles = subprocess.Popen(['psql', 'app', 'superuser', '-f', user_role_init_path], stdout = subprocess.PIPE)
    add_user_roles_exit_code = monitor_process(add_user_roles)

    if add_roles_exit_code != 0 and add_user_roles_exit_code != 0:
        plpy.error('--- Error while creating roles and user_role table ---')
    
    plpy.info('--- Finished creating roles and user_role ---')

    ###
    ## Create default user roles for school
    ###

    plpy.info('--- Create default user roles ---')

    add_default_data_functions = subprocess.Popen(['psql', 'app', 'superuser', '-f', default_data_path], stdout = subprocess.PIPE)
    add_default_data_functions_exit_code = monitor_process(add_default_data_functions)

    if add_roles_exit_code != 0 and add_user_roles_exit_code != 0:
        plpy.error('--- Error while creating default data functions ---')

    res_get_schools = plpy.execute("SELECT id FROM aula.school");
    
    for school in res_get_schools:
        # Create default roles
        add_default_roles_plan = plpy.prepare("""SELECT default_data.insert_default_roles($1)""", ["bigint"])
        res_add_default_roles = plpy.execute(add_default_roles_plan, [school["id"]])

    plpy.info('--- Finished creating default user roles ---')

    ###
    ## Alter users table
    ###
    
    plpy.info('--- Altering users table --- ')

    add_is_admin = plpy.execute("ALTER TABLE aula.users ADD COLUMN IF NOT EXISTS is_admin boolean NOT null DEFAULT false")
    add_is_school_admin = plpy.execute("ALTER TABLE aula.users ADD COLUMN IF NOT EXISTS is_school_admin boolean NOT null DEFAULT false")

    plpy.info('--- Finished altering users table --- ')

    ###
    ## Transfering and removing user_groups table
    ###

    plpy.info('--- Transfering and removing user_groups ---')
    
    get_user_groups = plpy.execute("SELECT us.id, us.school_id, ug.group_id, ug.idea_space FROM aula.users AS us JOIN aula.user_group AS ug ON us.id = ug.user_id;")

    for user_group in get_user_groups:
        user_id = user_group["id"]
        school_id = user_group["school_id"] 
        idea_space = user_group["idea_space"]
        user_group_name = user_group["group_id"]

        get_school_roles = plpy.prepare("SELECT id, name FROM aula.roles WHERE school_id = $1", ["bigint"])      
        res_get_school_roles = plpy.execute(get_school_roles, [school_id])

        insert_user_role = plpy.prepare("INSERT INTO aula.user_role(created_by, changed_by, user_id, school_id, role_id, idea_space) VALUES (1, 1, $1, $2, $3, $4)", ["bigint", "bigint", "bigint", "bigint"])
        get_user_role_id = plpy.prepare("SELECT id FROM aula.roles WHERE school_id = $1 AND name = $2", ["bigint", "text"])
    
        if user_group_name == 'admin':
            set_admin = plpy.prepare("UPDATE aula.users SET is_admin = true WHERE id = $1", ["bigint"])
            res_set_admin = plpy.execute(set_admin, [user_id])
            res_get_user_role_id = plpy.execute(get_user_role_id, [school_id, "Admin"])
            res_insert_user_role = plpy.execute(insert_user_role, [user_id, school_id, res_get_user_role_id[0]["id"], idea_space])
    
        elif user_group_name == 'school_admin':
            set_school_admin = plpy.prepare("UPDATE aula.users SET is_school_admin = true WHERE id = $1", ["bigint"])
            res_set_school_admin = plpy.execute(set_school_admin, [user_id])
            res_get_user_role_id = plpy.execute(get_user_role_id, [school_id, "Admin"])
            res_insert_user_role = plpy.execute(insert_user_role, [user_id, school_id, res_get_user_role_id[0]["id"], idea_space])

        elif user_group_name == 'student':
            res_get_user_role_id = plpy.execute(get_user_role_id, [school_id, "Student"])
            res_insert_user_role = plpy.execute(insert_user_role, [user_id, school_id, res_get_user_role_id[0]["id"], idea_space])
            
        elif user_group_name == 'moderator':
            res_get_user_role_id = plpy.execute(get_user_role_id, [school_id, "Moderator"])
            res_insert_user_role = plpy.execute(insert_user_role, [user_id, school_id, res_get_user_role_id[0]["id"], idea_space])

        elif user_group_name == 'principal':
            res_get_user_role_id = plpy.execute(get_user_role_id, [school_id, "Prüfer"])
            res_insert_user_role = plpy.execute(insert_user_role, [user_id, school_id, res_get_user_role_id[0]["id"], idea_space])

        elif user_group_name == 'school_guest':
            res_get_user_role_id = plpy.execute(get_user_role_id, [school_id, "Gast"])
            res_insert_user_role = plpy.execute(insert_user_role, [user_id, school_id, res_get_user_role_id[0]["id"], idea_space])
        else:
            res_get_user_role_id = plpy.execute(get_user_role_id, [school_id, "Gast"])
            res_insert_user_role = plpy.execute(insert_user_role, [user_id, school_id, res_get_user_role_id[0]["id"], idea_space])
            plpy.warning('No matching role for user (id: ' + str(user_id) + ', idea_space: ' + str(idea_space) + ') found. Inserting as guest now.')

    drop_user_groups = plpy.execute("DROP TABLE IF EXISTS aula.user_group");
    drop_group_id_type = plpy.execute("DROP TYPE IF EXISTS aula.group_id CASCADE");

    plpy.info('--- Finished transfering and removing user_groups ---')

    ###
    ## Update rpc functions    
    ###
    
    plpy.info('--- Updating rpc functions ---')

    update_rpc = subprocess.Popen(['psql', 'app', 'superuser', '-f', rpc_init_path], stdout = subprocess.PIPE)
    update_rpc_exit_code = monitor_process(update_rpc)

    if update_rpc_exit_code != 0:
        plpy.error('--- Error while updating rpc functions ---')

    plpy.info('--- Finished updating rpc functions ---')

    ###
    ## Update authorization
    ###

    plpy.info('--- Updating authorization functions ---')

    update_authorization = subprocess.Popen(['psql', 'app', 'superuser', '-f', authorization_init_path], stdout = subprocess.PIPE)
    update_authorization_exit_code = monitor_process(update_authorization)

    if update_authorization_exit_code != 0:
        plpy.error('--- Error while updating authorization functions ---')

    plpy.execute('DROP FUNCTION IF EXISTS aula.is_principal(bigint) CASCADE')
    plpy.execute('DROP FUNCTION IF EXISTS aula.is_moderator(bigint) CASCADE')
    
    # Commit changes
    plpy.commit()

    # Grant to aula_authenticator
    plpy.info('--- Granting access for aula_authenticator ---')
    grant_to_aula_authenticator = subprocess.Popen(['psql', 'app', 'superuser', '-f', base_dir + 'authorization/permissions.sql'], stdout = subprocess.PIPE)
    grant_to_aula_authenticator_exit_code = monitor_process(grant_to_aula_authenticator)
    
    if grant_to_aula_authenticator_exit_code != 0:
        plpy.error('--- Error while granting to aula_authenticator ---')

    plpy.info('--- Finished updating authorization functions ---')
     
    ###
    ## Update permissions
    ###

    plpy.info('--- Updating permissions ---')

    to_update_permissions = [
        ["idea", "aula.idea", "model/idea/permissions.sql"], 
        ["category", "aula.category", "model/category/permissions.sql"],
        ["comment", "aula.comment", "model/comment/permissions.sql"],
        ["comment_vote", "aula.comment_vote", "model/comment_vote/permissions.sql"],
        ["feasible", "aula.feasible", "model/feasible/permissions.sql"],
        ["idea_like", "aula.idea_like", "model/idea_like/permissions.sql"],
        ['idea_space', 'aula.idea_space', 'model/idea_space/permissions.sql'],
        ['idea_vote', 'aula.idea_vote', 'model/idea_vote/permissions.sql'],
        ['page', 'aula.page', 'model/page/permissions.sql'],
        ['school', 'aula.school', 'model/school/permissions.sql'],
        ['user_login', 'aula_secure.user_login', 'model/secure/user_login/permissions.sql'],
        ['topic', 'aula.topic', 'model/topic/permissions.sql'],
        ['users', 'aula.users', 'model/user/permissions.sql'],
        ]

    # Drop existing policies
    for to_update in to_update_permissions:
        plpy.info('--- Now updating ' + to_update[1] + " ---")
        existing_policies = plpy.prepare("SELECT policyname FROM pg_policies WHERE tablename = $1", ["text"])
        res_existing_policies = plpy.execute(existing_policies, [to_update[0]])
        dropped_policies = ''

        for policy in res_existing_policies:
            policy_name = policy["policyname"]
            dropped_policies += policy_name + ' '
            drop_existing_policies_query = "DROP POLICY IF EXISTS {policyName} ON {tableName}".format(policyName=policy_name, tableName=to_update[1])
            res_drop_exisiting_policies = plpy.execute(drop_existing_policies_query)

        # Commit dropped policies
        plpy.commit()
        plpy.info('--- Dropped policies: ' + dropped_policies + '---')

        # Add new policies
        update_permission = subprocess.Popen(['psql', 'app', 'superuser', '-f', base_dir + to_update[2]], stdout = subprocess.PIPE)
        update_permission_exit_code = monitor_process(update_permission)
        if update_permission_exit_code != 0:
            plpy.error('--- Error while updating permissions ---')     
    plpy.info('--- Finished updating permissions ---')
$$;
