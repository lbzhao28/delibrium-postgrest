DO language plpython3u $$
    import subprocess
    import io

    ###
    ## Set variables
    ###
    
    base_dir = '/docker-entrypoint-initdb.d/'
    check_permission_path = base_dir + 'authorization/check_permission.sql'
    insert_default_roles_path = base_dir +'libs/default_data/insert_default_roles.sql'

    # Helper function for printing output
    def monitor_process(process):
        for line in io.TextIOWrapper(process.stdout, encoding="utf-8"):
            plpy.info(line.rstrip())
        errcode = process.wait()
        return errcode

    ###
    ## Update check permission function
    ###

    plpy.info('--- Update check permission function ---')
   
    update_check_permission = subprocess.Popen(['psql', 'app', 'superuser', '-f', check_permission_path], stdout = subprocess.PIPE)
    update_check_permission_exit_code = monitor_process(update_check_permission)

    if update_check_permission_exit_code != 0:
        plpy.error('--- Error while updating check permission function ---')
    
    plpy.info('--- Finished updating check permission function ---')

    ###
    ## Update check instert default roles function
    ###

    plpy.info('--- Update insert default roles function ---')
   
    update_insert_default_roles = subprocess.Popen(['psql', 'app', 'superuser', '-f', insert_default_roles_path], stdout = subprocess.PIPE)
    update_insert_default_roles_exit_code = monitor_process(update_insert_default_roles)

    if update_insert_default_roles_exit_code != 0:
        plpy.error('--- Error while updating insert default roles function ---')
    
    plpy.info('--- Finished updating insert default roles function ---')

    ###
    ## Update existing default roles
    ###

    plpy.info('--- Update default role permissions ---')

    res_update_nutzer_permissions = plpy.execute("""UPDATE aula.roles SET permissions='{"vote_idea": ["school_and_idea_space"], "create_idea": ["school_and_idea_space"], "support_idea": ["school_and_idea_space"], "add_topic_idea": ["school_and_idea_space"], "create_comment": ["school_and_idea_space"], "support_comment": ["school_and_idea_space"]}' WHERE name='Nutzer';""")
    res_update_junior_moderator_permissions = plpy.execute("""UPDATE aula.roles SET permissions='{"edit_idea": ["idea_space"], "vote_idea": ["school_and_idea_space"], "edit_topic": ["idea_space"], "create_idea": ["school_and_idea_space"], "delete_idea": ["idea_space"], "create_topic": ["idea_space"], "delete_topic": ["idea_space"], "edit_comment": ["idea_space"], "support_idea": ["school_and_idea_space"], "edit_category": ["school"], "add_topic_idea": ["school_and_idea_space"], "create_comment": ["school_and_idea_space"], "delete_comment": ["idea_space"], "support_comment": ["school_and_idea_space"], "mark_idea_winner": ["idea_space"], "change_topic_phase": ["idea_space"]}' WHERE name='Junior-Moderator';""")
    res_update_pruefer_permissions = plpy.execute("""UPDATE aula.roles SET permissions='{"edit_topic": ["all"], "create_idea": ["all"], "delete_idea": ["all"], "approve_idea": ["all"], "create_topic": ["all"], "edit_category": ["school"], "add_topic_idea": ["all"], "create_comment": ["all"], "delete_comment": ["all"], "support_comment": ["all"], "mark_idea_winner": ["all"], "change_topic_phase": ["all"]}' WHERE name='Prüfer';""")
    res_update_admin_permissions = plpy.execute("""UPDATE aula.roles SET permissions='{"edit_idea": ["all"], "edit_page": ["school"], "edit_role": ["school"], "edit_room": ["school"], "edit_user": ["school"], "see_admin": ["school"], "edit_pages": ["school"], "edit_topic": ["all"], "create_idea": ["all"], "create_page": ["school"], "create_role": ["school"], "create_room": ["school"], "create_user": ["school"], "delete_idea": ["all"], "delete_page": ["school"], "delete_role": ["school"], "delete_room": ["school"], "delete_user": ["school"], "approve_idea": ["all"], "create_topic": ["all"], "delete_topic": ["all"], "edit_comment": ["all"], "edit_category": ["school"], "add_topic_idea": ["all"], "create_comment": ["all"], "delete_comment": ["all"], "create_category": ["school"], "delete_category": ["school"], "support_comment": ["all"], "mark_idea_winner": ["all"], "change_topic_phase": ["all"]}' WHERE name='Admin';""")
    res_update_moderator_permissions = plpy.execute("""UPDATE aula.roles SET permissions='{"edit_idea": ["all"], "edit_topic": ["all"], "create_idea": ["all"], "delete_idea": ["all"], "create_topic": ["all"], "delete_topic": ["all"], "edit_comment": ["all"], "edit_category": ["school"], "add_topic_idea": ["all"], "create_comment": ["all"], "delete_comment": ["all"], "support_comment": ["all"], "mark_idea_winner": ["all"], "change_topic_phase": ["all"]}' WHERE name='Moderator';""")

    plpy.info('--- Finished updating default role permissions ---')

$$;
