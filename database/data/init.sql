\set categories_path  `if [ :base_dir != "/docker-entrypoint-initdb.d" ]; then echo  "':base_dir/database/data/categories.csv'"; else echo "':base_dir/data/categories.csv'"; fi`
\set school_path `if [ :base_dir != "/docker-entrypoint-initdb.d" ]; then echo  "':base_dir/database/data/school.csv'"; else echo "':base_dir/data/school.csv'"; fi`
\set pages_path `if [ :base_dir != "/docker-entrypoint-initdb.d" ]; then echo  "':base_dir/database/data/pages.csv'"; else echo "':base_dir/data/pages.csv'"; fi`

-- Insert special school
copy aula.school (id,created_at, changed_at, name, config, created_by) from :school_path with (delimiter ',', format csv, quote '"', header true);
SELECT setval(pg_get_serial_sequence('aula.school','id'), coalesce(max("id"), 1), max("id") IS NOT null) FROM aula.school;

-- Insert admin user
insert into aula_secure.user_login (school_id, password) values ( 1, 'password');
insert into aula.users (school_id, user_login_id, first_name, last_name, changed_by, username, is_admin) values (1, 1, 'Admin', 'aula', 1, 'admin', True);
update aula_secure.user_login set aula_user_id = 1 where id = 1;

-- Insert student
insert into aula_secure.user_login (school_id, password) values ( 1, 'password');
insert into aula.users (school_id, user_login_id, first_name, last_name, username, changed_by) values (1, 2, 'Student', 'Example', 'student', 1);
update aula_secure.user_login set aula_user_id = 2 where id = 2;

-- Insert default roles
SELECT default_data.insert_default_roles(1);

-- Insert Class
insert into aula.idea_space (school_id, created_by, changed_by, title, description, slug) values (1,1,1,'Klasse', 'Test Klasse', 'test-klasse');

-- Import categories
copy aula.category (id,school_id, name, description, image,def,position) from :categories_path with (delimiter ',', format csv, quote '"', header true);
SELECT setval('aula.category_id_seq', 7, true);
-- SELECT setval(pg_get_serial_sequence('aula.category','id'), coalesce(max("id"), 1), max("id") IS NOT null) FROM aula.school;

-- Import pages
copy aula.page (id,created_by, created_at,changed_by, changed_at,school_id, name, public,content,config) from :pages_path with (delimiter ',', format csv, quote '"', header true);
SELECT setval(pg_get_serial_sequence('aula.page','id'), coalesce(max("id"), 1), max("id") IS NOT null) FROM aula.school;
